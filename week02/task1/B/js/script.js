const calculateDaysInMonth = month => {
    if (month == 2) {
        let year = new Date().getFullYear();
        if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
            return 29;
        } else {
            return 28;
        }
    }
    else if (month == 4 || month == 6 || month == 9 || month == 11) {
        return 30;
    }
    else if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
        return 31
    }
    else {
        return "Такого месяца не существует";
    }
}

document.getElementById('calculateBtn').addEventListener('click', () => {
    let monthInput = document.getElementById('monthInput').value;
    let result = calculateDaysInMonth(monthInput);
    document.getElementById('result').textContent = 'В этом месяце: ' + result + ' дней';
});