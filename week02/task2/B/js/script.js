const checkPrime = () => {
    const number = document.getElementById("numberInput").value;
    const isPrime = isNumberPrime(Number(number));
    displayResult(number, isPrime);
};

const isNumberPrime = (number) => {
    if (number <= 1) {
        return false;
    } else {
        for (let i = 2; i <= Math.sqrt(number); i++) {
            if (number % i === 0) {
                return false;
            }
        }
    }
    return true;
};

const displayResult = (number, isPrime) => {
    const resultElement = document.getElementById("result");

    if (isPrime) {
        resultElement.innerText = number + " это простое число";
    } else {
        resultElement.innerText = number + " это не простое число";
    }
};

document.getElementById("result", checkPrime);