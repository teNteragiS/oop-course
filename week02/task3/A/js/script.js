const countMaxNumbers = () => {
    let numbersInput = document.getElementById('numbers');
    let numbers = numbersInput.value.split(',').map(Number);

    let maxNumber = Math.max(...numbers);
    let count = 0;

    for (let i = 0; i < numbers.length; i++) {
        if (numbers[i] === maxNumber) {
            count++;
        }
    }

    let resultText = "Количество чисел, равное максимальному, равно: " + count;

    let resultElement = document.getElementById('result');
    resultElement.innerText = resultText;
}