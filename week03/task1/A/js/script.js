const countOccurrences = () => {
  const subString = document.getElementById('subString').value;
  const strings = document.getElementById('strings').value.split('\n');

  let count = 0;
  strings.forEach(text => {
    count += countOccurrencesInString(text, subString);
  });

  displayResult(count);
};

const countOccurrencesInString = (text, subString) => {
  let count = 0;
  let position = text.indexOf(subString);

  while (position !== -1) {
    count++;
    position = text.indexOf(subString, position + 1);
  }

  return count;
};

const displayResult = count => {
  const result = document.getElementById('result');
  result.innerText = 'Количество вхождений: ' + count;
};