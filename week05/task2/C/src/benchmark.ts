// const fs = require('fs');
// const XLSX = require('xlsx');

// // Функция для рекурсивного вычисления числа Фибоначчи
// const fibonacciRecursive = (n) => {
//   if (n <= 1) {
//     return n;
//   }
//   return fibonacciRecursive(n - 1) + fibonacciRecursive(n - 2);
// };

// // Функция для оптимизированного вычисления числа Фибоначчи с кэшированием
// const fibonacciOptimized = (n, cache) => {
//   if (cache.has(n)) {
//     return cache.get(n);
//   }
//   if (n <= 1) {
//     return n;
//   }
//   const result = fibonacciOptimized(n - 1, cache) + fibonacciOptimized(n - 2, cache);
//   cache.set(n, result);
//   return result;
// };

// // Создание тестовых данных
// const generateTestData = (n) => {
//   const testData = [];
//   for (let i = 0; i < n; i++) {
//     const startTimeRecursive = process.hrtime();
//     const recursiveResult = fibonacciRecursive(i);
//     const endTimeRecursive = process.hrtime(startTimeRecursive);
//     const recursiveExecutionTime = (endTimeRecursive[0] * 1000 + endTimeRecursive[1] / 1000000).toFixed(5);

//     const startTimeOptimized = process.hrtime();
//     const cache = new Map();
//     const optimizedResult = fibonacciOptimized(i, cache);
//     const endTimeOptimized = process.hrtime(startTimeOptimized);
//     const optimizedExecutionTime = (endTimeOptimized[0] * 1000 + endTimeOptimized[1] / 1000000).toFixed(5);

//     testData.push({ Число: i, 'Время рекурсии': recursiveExecutionTime, 'Время оптимизированно': optimizedExecutionTime });
//   }
//   return testData;
// };

// // Количество чисел Фибоначчи для генерации
// const n = 50;

// // Генерация тестовых данных
// const testData = generateTestData(n);

// // Создание новой книги Excel
// const workbook = XLSX.utils.book_new();

// // Создание нового листа в книге
// const worksheet = XLSX.utils.json_to_sheet(testData);

// // Добавление листа в книгу
// XLSX.utils.book_append_sheet(workbook, worksheet, 'Данные');

// // Запись книги в файл
// XLSX.writeFile(workbook, 'fibonacci_test_data3.xlsx');

// console.log(`Excel файл с тестовыми данными создан: fibonacci_test_data.xlsx`);