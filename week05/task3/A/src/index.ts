const removeDuplicates = () => {
  const inputElement = document.getElementById("input") as HTMLInputElement;
  const inputArray = inputElement.value.split(" ").map(Number);

  const uniqueArray = removeDuplicatesFromArray(inputArray);

  displayOutput(uniqueArray);
};

const removeDuplicatesFromArray = (array: number[]): number[] => {
  const set = new Set(array);
  return Array.from(set);
};

const displayOutput = (array: number[]) => {
  const outputElement = document.getElementById("output") as HTMLInputElement;
  outputElement.innerHTML = array.join(", ");
};